import { Component } from '@angular/core';
import { UserService} from './user.service';
import { createOfflineCompileUrlResolver } from '@angular/compiler';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'countries';
  data=[];
  

  constructor(private user:UserService)
  {
    this.user.getData().subscribe(data=>{
      console.warn(data)
    })
  }
}
   