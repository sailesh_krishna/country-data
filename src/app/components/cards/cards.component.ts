import { Component, OnInit } from '@angular/core';
import { UserService} from 'src/app/user.service';
@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {
data;
  constructor(private user:UserService) { 
    this.user.getData().subscribe(data=>{
      const res = data as any;
      this.data = res
      console.log(this.data, 'data')
  })
  }
  ngOnInit(): void {
  }

}
